package mehAgain;

import java.io.*;
import java.util.Scanner;

public class Binary {
	public static void main(String[] args) {
        int num;
        System.out.println("Enter a number:");
	    Scanner sc = new Scanner(System.in);
	    num = sc.nextInt();
	
        /*
             Your logic goes here
        */
	    int test = num; int meh;
	    boolean bin = true;
	    while (test > 0) {
	    	if (!(test % 10 == 1 || test % 10 == 0)) {
	    		bin = false;
	    		break;
	    	}
	    	test /= 10;
	    }
	    
	    if (bin) {
	    	System.out.println("Binary");
	    } else {
	    	System.out.println("Not Binary");
	    }
	
	}
}
