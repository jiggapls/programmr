package mehAgain;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Palindrome {

	public static void main(String[] args) {
		String strn = "";  
        int flag=0;  
        System.out.println("Enter the string:");  
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));  
        try {
			strn=br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
        System.out.println("Result string is:");  
        //write your logic here  
         for (int i = 0; i < strn.length()/2; i++) {
             if (strn.charAt(i) == strn.charAt(strn.length() - 1 - i))
                 flag = 1;
             else {
                 flag = 0;
                 break;
             }
         }
        //end  
        if(flag==1)  
            System.out.print("palindrome");  
        else   
            System.out.print("not a palindrome2");

	}

}
