package mehAgain;

import java.io.*;

public class DigitSum {

	public static void main(String[] args) throws Exception {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the date(ddmmyy):");
		int number =Integer.parseInt(br.readLine());
		int result = 0;
		
		while (number > 0 || result > 9) {
			result += number % 10;
			number /= 10;
			if (number == 0 && result > 9) {
				number = result;
				result = 0;
			}
		}
		
		System.out.println("Your lucky number is: \n" + result); 
		
	}
}
