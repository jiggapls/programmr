package mehAgain;

import java.io.*;
import java.util.*;

public class PrintRect {

	public static void main(String[] args) throws Exception{
		int l,w;
        Scanner scan=new Scanner(System.in);
        System.out.println("Enter the length:");
        l=scan.nextInt();
        System.out.println("Enter the width:");
        w=scan.nextInt();
   
        //Write your logic here

        //Print the top of the rectangle
        for (int i = 0; i < l; i++)
        	System.out.print("*");
        System.out.println();
        
        //Print the sides of the rectangle
        for (int i = 0; i < w-2; i++) {
        	System.out.print("*");
        	for (int j = 0; j < l-2; j++)
        		System.out.print(" ");
        	System.out.println("*");
        }
        
        //Print the bottom of the rectangle
        for (int i = 0; i < l; i++) 
        	System.out.print("*");
        
        //end

	}

}
