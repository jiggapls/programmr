package mehAgain;

import java.io.*;
import java.util.*;

public class NaturalSum {
	public static void main(String[] args) {
		int max=0;
		int sum = 0;

		System.out.println("Enter the maximum no:");
		Scanner sc = new Scanner(System.in);
		max = sc.nextInt();

		/* Your logic goes here */
		if (max < 3)
			;
		else {
			for (int i = 3; i < max; i++) {
				if (i % 3 == 0 || i % 5 == 0)
					sum += i;
			}
		}
		
		System.out.println("Total is: \n" + sum);

	}
}