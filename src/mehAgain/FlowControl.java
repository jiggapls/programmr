package mehAgain;

import java.util.*;
import java.io.*;

public class FlowControl {

	public static void main(String[] args) {
		System.out.println("Input a number to test if it is prime: ");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		String result = "";
		
		if (num <= 2) {
			result = "The number is prime.";
		} else { 
			for (int i = 2; i < Math.sqrt(num); i++) {
				if (num % i == 0) {
					result = "The number is not prime.";
					break;
				} else
					result = "The number is prime.";
			}
		}
		
		System.out.println(result);
	}

}
