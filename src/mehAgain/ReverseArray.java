package mehAgain;

import java.io.*;
import java.util.*;

public class ReverseArray {
	public static void main(String a[]) throws Exception {
		//write your logic here
		int temp;
		
		int arr[] = new int[10];
		Scanner scan= new Scanner(System.in);
		for(int i =0;i<10;i++)
		{
			System.out.println("Enter the array elements["+i+"]:");
			arr[i]=scan.nextInt();
		} 
		//write your logic here

		for (int i = 0; i < arr.length/2; i++) {
			temp = arr[i];
			arr[i] = arr[arr.length-1-i];
			arr[arr.length-1-i] = temp;
		}
		
		//end 

		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]+" ");
		}

	}
}
