package mehAgain;

import java.io.*;
import java.util.Scanner;

public class StatementReversal {
	public static void main(String a[]) throws Exception
	{
		String strArr[];    //array which will hold splitted strings of the statement
		String st;      //contains user input statement 
		String reverse = "";
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Statement:"); 
		st=br.readLine();
		System.out.println("The reverse is:");

		/*write down your logic here*/ 
		
		strArr = st.split(" ");
		
//		System.out.println(st);
		for (int i = strArr.length-1; i >= 0; i--)
			reverse += strArr[i] + " ";
		System.out.println(reverse);
		
	} 
}
