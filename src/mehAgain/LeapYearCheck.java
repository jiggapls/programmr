package mehAgain;

import java.util.Scanner;

public class LeapYearCheck {

	public static void main(String[] args) {
		int year = 0;

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Year:"); 
		year = sc.nextInt();

		/*
	           Your code goes here
		 */
		String result = "";
		if (year % 4 == 0)
			result = "Leap";
		else
			result = "Not leap";

		System.out.println("Year is: \n" + result); 
	}

}
