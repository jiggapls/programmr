package mehAgain;

import java.util.*;
import java.io.*;

public class ReverseTriangle {

	public static void main(String[] args) {
		System.out.println("Enter a number: ");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		
		for (int i = num; i > 0; i--) {
			int dashes = num - i;
			for (int j = 1; j <= i; j++) {
				while (dashes > 0) {
					System.out.print("-");
					dashes--;
				}
				System.out.print(j);
			}
			System.out.println();
		}
	}

}
