package mehAgain;

import java.io.*;
import java.util.Scanner;

public class ToUpperCase {

	public static void main(String[] args) throws IOException {
		
		String str; String upper = "";
	    
	    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	    System.out.println("Enter a string:"); 
	    str = br.readLine();

	    /*
	       Your Logic is here
	    */
	    
	    for (int i = 0; i < str.length(); i++) {
	    	if ((int)str.charAt(i) >= 97 && (int)str.charAt(i) <= 122) 
	    		upper += (char)((int)str.charAt(i) - 32);
	    	else
	    		upper += str.charAt(i);
	    }
	    
//	    System.out.println("Value of char ' ' is " + (int)' ');
//	    System.out.println("Value of char '!' is " + (int)'!');
//	    System.out.println("Value of char 'a' is " + (int)'a');
//	    System.out.println("Value of char 'z' is " + (int)'z');
//	    System.out.println("Value of char 'A' is " + (int)'A');
//	    System.out.println("Value of char 'Z' is " + (int)'Z');

	    System.out.println("Uppercase conversion: \n" + upper);
		
	}

}
