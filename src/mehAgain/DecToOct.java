package mehAgain;

import java.io.*;
import java.util.Scanner;

public class DecToOct {

	public static void main(String[] args) {
		int num,rem,oct = 0; int dec = 0;
	    Scanner sc = new Scanner(System.in);
	    System.out.println("Enter a number:");
	    num = sc.nextInt();
		
	    /*
	        Your code goes here
	    */
	    
		while (num != 0) {
			rem = num % 8;
			num /= 8;
			oct += rem * Math.pow(10, dec);
			dec++;
		}
		
//		rem = 10 % 8; num = 10 / 8;
//	    System.out.println("modulo is " + rem);
//	    System.out.println("divide is " + num);
	    
		System.out.println("Octal number is: \n" + oct);

	}

}
