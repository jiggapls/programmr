package mehAgain;

import java.io.*;
import java.util.*; 

public class AvgPosNeg {
	public static void main(String a[]) throws Exception
	{
		int[] arr = new int[10];

		double avg_pos=0.0,avg_neg=0.0;
		Scanner scan=new Scanner(System.in);

		for(int i=0;i<10;i++) {
			System.out.println("Enter Number ["+i+"]:");
			arr[i]=scan.nextInt();  
		}

		//Write your logic here
		int num = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] < 0.0) {
				avg_neg += arr[i];
				num++;
			} else
				avg_pos += arr[i];
		}
		avg_neg /= num;
		avg_pos /= (arr.length - num);
		
		//end
		System.out.println("positivenumbers:\n"+avg_pos);
		System.out.println("negativenumbers:\n"+avg_neg);
	}
}
