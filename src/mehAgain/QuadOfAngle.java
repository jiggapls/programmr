package mehAgain;

import java.io.*;
import java.util.Scanner;
import java.lang.Math;

public class QuadOfAngle
{
   /**
      Calculate the quadrant of an angle.
   */
   public static void main (String[] args) {
      
        int angle = 0;
        System.out.println("Type an integer angle (in degrees) and press Enter:");
        Scanner sc = new Scanner(System.in);
        angle= sc.nextInt();
        
        int quadrant;
        
        /*
         Your code goes here
        */
        
        quadrant = ((angle % 360) / 90) + 1;
        
        // output
        System.out.println("Quadrant is: \n" + quadrant);
     
   }
} 


